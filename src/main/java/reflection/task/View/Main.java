package reflection.task.View;


import reflection.task.Controller.Controller;

public class Main {

    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.realisation();
    }
}